import random


def f(x, mu):
    """Logistic map (LM)"""
    return mu * x * (1 - x)


def mapping(x_0, mu, n):
    """sequence generation (single)"""
    resulted_map = []
    x = x_0
    for _ in range(0, n):
        x = f(x, mu)
        resulted_map.append(x)
    return resulted_map


def f_sync(x, y, mu, eps):        # function for sync mapping
    """sync of two sequences"""
    f_x = f(x, mu)
    f_y = f(y, mu)
    return f_x - eps * (f_x - f_y), f_y + eps * (f_x - f_y)


def f_sync_transformed(x, y):
    """to (u,v)-coordinates"""
    u_n = [(x + y) / 2 for (x,y) in zip(x,y)]
    v_n = [(x - y) / 2 for (x,y) in zip(x,y)]
    return u_n, v_n


def sync_mapping(x_0, y_0, n, mu, eps):
    """sequence generation for pair"""
    x_n = []
    y_n = []
    x = x_0
    y = y_0

    for _ in range(n):
        x, y = f_sync(x, y, mu, eps)
        x_n.append(x)
        y_n.append(y)

    return x_n, y_n


def x_0_arr_generation():  # (0; 0.25), (0.25; 0.5), (0.5; 0.75), (0.75; 1) - intervals for (1)
    """ for part 1 - random initial values"""
    return [random.uniform(0, 0.25), random.uniform(0.25, 0.5), random.uniform(0.5, 0.75), random.uniform(0.75, 1)]


def eps_arr_generation():
    """ for part 2 - random connection parameter value"""
    return [random.uniform(0, 0.5) for _ in range(10)]
