from scripts import *


def main():
    mu = 3.7
    n = 1000

    # step 1,2
    # step_1_2_script(mu, n)

    # step 3, 4
    step_3_4_script(mu, n)

if __name__ == "__main__":
    main()