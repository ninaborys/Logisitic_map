from tools import *
from plotting import *
import math


def step_1_2_script(mu, n):
    x_0_arr = x_0_arr_generation()
    for x_0 in x_0_arr:
        print('x_0 = ', x_0)
        x_n = mapping(x_0, mu, n)  # {x_n} - sequence for random x_0 from interval
        plot_x_n_from_n(x_n, n, 'x_n from n')
        plot_x_new_from_x_n(x_n, n)
    return


def step_3_4_script(mu, n):
    eps_arr = eps_arr_generation()

    x_0 = random.uniform(0, 1)
    y_0 = random.uniform(0, 1)
    if x_0 == y_0:
        pass
    else:
        print("x_0 = ", x_0)
        print("y_0 = " , y_0)
        for eps in eps_arr:
            print("eps = ", eps)
            x_n, y_n = sync_mapping(x_0, y_0, n, mu, eps)
            plot_dependence(x_n, y_n, 'y_n from x_n')
            u_n, v_n = f_sync_transformed(x_n, y_n)
            plot_dependence(u_n, v_n, 'v_n from u_n')
            v_n = [v for v in v_n if v != 0]

            ln_v_n = [math.log(abs(v)) for v in v_n]
            plot_x_n_from_n(ln_v_n, len(v_n), 'ln |v_n| from n')
            ln_v_n_on_n = [math.log(abs(e)) / (i + 1) for i, e in enumerate(v_n)]
            plot_x_n_from_n(ln_v_n_on_n, len(v_n), 'ln |v_n| / n from n')
    return