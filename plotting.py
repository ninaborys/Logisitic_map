import matplotlib.pyplot as plt


def plot_x_n_from_n(x_n, n, label):
    n_arr = [i for i in range(n)]
    plt.plot(n_arr, x_n, 'r.-', label=label)
    plt.title(label)
    plt.xlim([0, 1000])
    plt.grid(True)
    plt.show()


def plot_x_new_from_x_n(x_n,n):
    plt.plot(x_n[0:n-1], x_n[1:n], 'b.-', label='x_{n+1}(x_n)')
    plt.title('x_{n+1} from (x_n) dependence')
    plt.grid(True)
    plt.show()


def plot_dependence(x, y, label):
    plt.plot(x, y, 'g.', label=label)
    plt.title(label)
    plt.grid(True)
    plt.show()

